<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  
  'title' => t("Date context from date argument"),
//  'keyword' => 'relcontext',
  'description' => t('Adds a date context with data from a date context'),
  'required context' => new ctools_context_required(t('Date context'), 'date_context'),
  'context' => 'ctools_date_context_from_date_arg_context',
  'settings form' => 'ctools_date_context_from_date_arg_settings_form',
  
);

/**
 * Return a new context based on an existing context.
 */
function ctools_date_context_from_date_arg_context($context = NULL, $conf) {

  return ctools_context_create('date_context', $context, $conf);
}

/**
 * Settings form for the relationship.
 */
function ctools_date_context_from_date_arg_settings_form($conf) {

  $form = array();
  $form['modify_date_str'] = array(
    '#type' => 'textfield',
    '#title' => t('String to modify date argument'),
    '#size' => 50,
    '#description' => t('String to modify the date argument. Uses <a href="@strtotime-link">strtotime</a> syntax. Ex: +1 day. Leave empty to not modify date argument', array('@strtotime-link' => 'http://php.net/manual/en/function.strtotime.php' )),
    '#default_value' => $conf['modify_date_str'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  $form['date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#size' => 50,
    '#description' => t('Format to output the date in. Uses PHP <a href="@date-link">Date</a> syntax. Leave empty to use <i>@date-example</i> (Y-m-d)', array('@date-link' => 'http://php.net/manual/en/function.date.php', '@date-example' => date('Y-m-d') )),
    '#default_value' => $conf['date_format'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  $form['reldate_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Relative date format'),
    '#size' => 50,
    '#description' => t('Format to output the relative date. Uses PHP <a href="@date-link">Date</a> syntax. Leave empty to use <i>@date-example</i> (D j F)', array('@date-link' => 'http://php.net/manual/en/function.date.php', '@date-example' => date('D j F') )),
    '#default_value' => $conf['reldate_format'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  return $form;
}