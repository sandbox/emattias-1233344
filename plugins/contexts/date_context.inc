<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Date context"),
  'description' => t('A single "date_context" context, or data element.'),
  'context' => 'date_context_create_date_context',  // func to create context
  'context name' => 'date_context',
  'keyword' => 'date_context',

  // Provides a list of items which are exposed as keywords.
  'convert list' => 'date_context_convert_list',
  // Convert keywords into data.
  'convert' => 'date_context_convert',

  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter some data to represent this "date_context".'),
  ),
);

/**
 * Create a context, either from manual configuration or from an argument on the URL.
 *
 * @param $empty
 *   If true, just return an empty context.
 * @param $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param $conf
 *   TRUE if the $data is coming from admin configuration, FALSE if it's from a URL arg.
 *
 * @return
 *   a Context object/
 */
function date_context_create_date_context($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('date_context');
  $context->plugin = 'date_context';
  $date_format = $conf['date_format'] ? $conf['date_format'] : 'Y-m-d';
  $timestamp = $data->original_argument ? strtotime($data->original_argument) : time();

  if ($empty) {
    return $context;
  }

  if($conf){

    if($conf['modify_date_str']){

      $timestamp = strtotime($conf['modify_date_str'], $timestamp);
      
    }

  }

  $context->title = t("Date context");
  $context->data = new stdClass();
  $context->argument = format_date($timestamp, 'custom', $date_format);
  $context->data->date_argument = $context->argument;
  $context->data->date_reldate = date_context_relative_date($timestamp, $conf);
  $context->data->date_timestamp = $timestamp;

  return $context;
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function date_context_convert_list() {
  return array(
    'date_argument' => t('Date argument (formatted)'),
    'date_timestamp' => t('Date timestamp'),
    'date_reldate' => t('Relative date')
  );
}

/**
 * Convert a context into a string to be used as a keyword by content types, etc.
 */
function date_context_convert($context, $type) {
  switch ($type) {
    case 'date_argument':
      return $context->data->date_argument;
    case 'date_timestamp':
      return t($context->data->date_timestamp);
    case 'date_reldate':
      return t($context->data->date_reldate);
  }
}

/**
 *  Pass a time / date string in strtotime format and the function will return a string with a date relative to today.
 */

function date_context_relative_date($time, $conf) {

  $today = strtotime(date('M j, Y'));

  $reldays = ($time - $today)/86400;

  if ($reldays >= 0 && $reldays < 1) {

    return t('Today');

  } else if ($reldays >= 1 && $reldays < 2) {

    return t('Tomorrow');

  } else if ($reldays >= -1 && $reldays < 0) {

    return t('Yesterday');

  }else{

    $reldate_format = $conf['reldate_format'] ? $conf['reldate_format'] : 'D j F';
    return format_date($time, 'custom', $reldate_format);
    
  }

}
